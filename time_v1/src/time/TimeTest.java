package time;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author Abdi_
 * 
 */

public class TimeTest {
	
	@Test
	public void getTotalSecondsRegular() {
		int total = Time.getTotalSeconds("01:01:50");
		System.out.println("total: " + total);
		assertTrue("Does the total seconds match?", total == 3710);
	}
	
	@Test(expected=NumberFormatException.class)
	public void getTotalSecondsException() {
		int total = Time.getTotalSeconds("00:59:B1");
		fail("The time entered is not valid. Try again!");
	}
	
	@Test
	public void getTotalSecondsBI() {
		int total = Time.getTotalSeconds("00:00:59");
		assertTrue("Check if boundary in-case works?", total == 59);
	}
	
	@Test(expected=NumberFormatException.class)
	public void getTotalSecondsBoundaryOut() {
		int total = Time.getTotalSeconds("00:00:60");
		fail("The time entered is not valid. Try again!");
	}
	
	/*
	@Test(expected=NumberFormatException.class)
	public void getTotalSecondsBO() {
		int total = Time.getTotalSeconds("00:00:60");
		fail("The time entered is not valid. Try again!");
	}
	
	@Test(expected=StringIndexOutOfBoundsException.class)
	public void getTotalSecondsException() {
		int total = Time.getTotalSeconds("00:59:B");
		fail("The time entered is not valid. Try again!");
	}
	
	public void getTotalSeconds() {
		assertTrue("Does the total seconds match?", Time.getTotalSeconds("00:01:59") != 3540);
	}
	
	
	public void getTotalSecondsBI() {
		assertTrue("Check if boundary in-case works?", Time.getTotalSeconds("00:00:59") == 59);
	}
	*/
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Boundary out: pass scenario.
	 * Test the milliseconds portion of the string.
	 * This test will use four digits for the milliseconds.
	 * Expected: Wrong number of milliseconds returned.
	 * Solution: I refactored the code for the program to assume if length is > 12, it will return an error
	 * It will check for the last few digits and if it is > 999, it will return a NumberFormatException.
	 */
	/**
	@Test(expected=NumberFormatException.class)
	public void getMillisecondsBoundaryOut() {
		int ms = Time.getMilliseconds("12:05:05:1000");
		assertEquals("Invalid milliseconds", ms == 1000);
	}
	
	@Test()
	public void getMillisecondsRegular() {
		int ms = Time.getMilliseconds("12:05:05:05");
		assertTrue("The milliseconds matches and works as intended.", ms == 5);
	}

	@Test(expected=NumberFormatException.class)
	public void getMillisecondsException() {
		int ms = Time.getMilliseconds("12:05:05:0A");
		fail("Invalid number of milliseconds");
	}
	
	@Test()
	public void getMillisecondsBoundaryIn() {
		int ms = Time.getMilliseconds("12:05:05:999");
		assertTrue("Does the milliseconds match?", ms == 999);	
		}
	
	*/
}

